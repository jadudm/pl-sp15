;; This is written in the full Racket language.
#lang racket
(require rackunit rackunit/text-ui)

;; I'll use this definition so tests fail.
;; Replace it with a correct expression in the questions below.
(define FIXME 'FIXME)
 
;; You can paste this Gist into Dr. Racket and 
;; complete your definitions there.
;; 3 + 5
(define ip1 FIXME)
 
;; 4 + 2 * 2
(define ip2 FIXME)
 
;; (2 * 9) - (20 / 2)
(define ip3 FIXME)
 
;; ((3 + 14) * 2) - 30
(define ip4 FIXME)
 
;; sin(90) * 8
(define ip5 FIXME)
 
;; 2^100
;; (This is exponentiation. You can use the "Help Desk" 
;; to search for the function you want.)
(define ip6 FIXME)
 
;; (2^100 * 3^100)
(define ip7 FIXME)
 
;; 5 + (3^17 % 8)
(define ip8 FIXME)

;; Tests
(define-test-suite infix-suite
  (check-equal? ip1 8)
  (check-equal? ip2 8)
  (check-equal? ip3 8)
  (check-equal? ip4 8)
  (check-equal? ip5 8)
  (check-equal? ip6 1267650600228229401496703205376)
  (check-equal? ip7 515377520732011331036461129765621272702107522001/1267650600228229401496703205376)
  (check-equal? ip8 8))
(run-tests infix-suite)

