---
---

## A Brief History

There are three documents I'd like you to spend some time investigating.

* [The History of Programming Languages](http://archive.oreilly.com/pub/a/oreilly//news/languageposter_0504.html)

    Grab the PDF.

* [A Brief, Incomplete, and Mostly Wrong History of Programming Languages](http://thequickword.wordpress.com/2014/02/16/james-irys-history-of-programming-languages-illustrated-with-pictures-and-large-fonts/)

    We'll do some reading about some of these people next week...

* [WAT](https://www.destroyallsoftware.com/talks/wat)

    And, as we learn more about languages, we'll see why this is hilarious. Well, sorta. I mean, "hilarious" in the context of "most of your friends would not only not get this, but they'd probably really think you were weird because you thought this was funny." 
